import java.util.Scanner;
public class searchsorted_lab3_2 {
    public static int checkArray(int[] a,int target){
        for(int i=0;i<a.length;i++){
            if(a[i]==target){
                return i;
            }
        }
        return -1;
    }
    public static void main(String[] args) {
        int[] ar = {4,5,6,7,0,1,2};
        int target = 0;
        System.out.println("Ex1");
        System.out.println("Array = {4,5,6,7,0,1,2} target = "+target);
        System.out.println("Output: "+checkArray(ar,target));
        System.out.println();
        int[] ar2 = {4,5,6,7,0,1,2};
        int target2 = 3;
        System.out.println("Ex2");
        System.out.println("Array = {4,5,6,7,0,1,2} target = "+target2);
        System.out.println("Output: "+checkArray(ar,target2));
        System.out.println();
        int[] ar3 = {1};
        int target3 = 0;
        System.out.println("Ex3");
        System.out.println("Array = {4,5,6,7,0,1,2} target = "+target3);
        System.out.println("Output: "+checkArray(ar,target3));
    }
}
